FROM python:3.8-slim

WORKDIR /code

ADD requirements.txt /requirements.txt

RUN pip install -r /requirements.txt

ADD src/* /code/


ENTRYPOINT ["python", "-u", "main.py", "--config", "/config/config.json", "--config-schema", "config.schema.json"]


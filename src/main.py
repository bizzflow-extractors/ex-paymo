"""Paymo extractor runner"""

import os
import logging
import csv
from math import floor
from datetime import datetime, timedelta
from typing import Dict, List
from bizztreat_base.config import Config
from paymo_client import PaymoClient
from schemas import schemas

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def save_csv(endpoint: str, data: List[Dict], output_dir: str = "/data/out/tables"):
    fname = os.path.join(output_dir, f"{endpoint}.csv")
    logger.info("Exporting %s to %s", endpoint, fname)
    with open(fname, "w", encoding="utf-8") as fid:
        writer = csv.DictWriter(fid, fieldnames=schemas[endpoint], dialect=csv.unix_dialect, extrasaction="ignore")
        writer.writeheader()
        writer.writerows(data)


def interval_to_filter(interval: int) -> str:
    from_date = datetime.now() - timedelta(days=interval)
    return f"created_on >= {floor(from_date.timestamp())}"


def main():
    proxy = Config(force_schema=True)
    config = proxy.config

    if not os.path.exists(proxy.args.output):
        os.makedirs(proxy.args.output)

    client = PaymoClient(token=config.get("token"), user=config.get("user"), password=config.get("password"))
    for settings in config.get("endpoints", []):
        logger.info("Extracting %s", settings["endpoint"])
        interval = settings.get("interval")
        if settings["endpoint"] in ["entries", "tasks"]:
            if not interval:
                from_ = datetime.now().replace(day=1, month=1, minute=0, second=0, hour=0, microsecond=0)
            else:
                from_ = datetime.now() - timedelta(days=interval)
            pids = [str(project["id"]) for project in client.get_projects()]
            pids_str = ", ".join(pids)
            default_filters = [f"project_id in ({pids_str})"]
            data = list(
                client.get_range(settings["endpoint"], *settings.get("filters", default_filters), from_=from_)
            )
        else:
            filters = settings.get("filters", [])
            if interval:
                filters.append(interval_to_filter(interval))
            data = client.get_endpoint(settings["endpoint"], *filters)
        save_csv(settings["endpoint"], data, proxy.args.output)


if __name__ == "__main__":
    main()

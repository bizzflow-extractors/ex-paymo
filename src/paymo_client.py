"""Paymo client
"""

from datetime import datetime, timedelta
from math import floor, ceil
import time
import logging
from urllib.parse import urljoin
from typing import List, Optional, Dict

import requests
from requests.auth import HTTPBasicAuth

logger = logging.getLogger(__name__)


class ClientError(Exception):
    """Raised on client error"""


class PaymoClient:
    def __init__(
        self,
        token: Optional[str] = None,
        user: Optional[str] = None,
        password: Optional[str] = None,
        base_url: str = "https://app.paymoapp.com/api",
    ):
        if not any((token, all((user, password)))):
            raise ClientError("Either token or user and password must be set")
        self.user = user or token
        self.password = password or "password"  # according to documentation, that may be anything
        self.base_url = base_url.rstrip("/")
        self.session = requests.Session()
        self.session.auth = HTTPBasicAuth(username=self.user, password=self.password)

    @staticmethod
    def _process_filters(*filters) -> Dict[str, str]:
        return {"where": " and ".join(filters)}

    def _get(self, endpoint, *filters, **kwargs) -> requests.Response:
        url = urljoin(f"{self.base_url}/", endpoint.lstrip("/"))
        logger.info("API Endpoint: %s", url)
        params = self._process_filters(*filters)
        params.update(kwargs.pop("params", {}))
        logger.debug(params)
        return self._armored_get(url, params=params, **kwargs)

    def _armored_get(self, url, params, **kwargs) -> requests.Response:
        response = self.session.get(url, params=params, **kwargs)
        if response.status_code == 429:
            after: str = response.headers.get("retry-after") or "30"
            logger.info("Rate limited, waiting for %s seconds", after)
            time.sleep(int(after))
            return self._armored_get(url, params, **kwargs)
        if response.status_code == 502:
            logger.info("Got 502 Bad Gateway, retrying")
            return self._armored_get(url, params, **kwargs)
        response.raise_for_status()
        return response

    def get_range(self, endpoint, *filters, from_: datetime):
        logger.info("Downloading %s for dates from %s", endpoint, from_)
        from_unix = floor(from_.timestamp())
        date_filters = []
        date_filters.append(f"created_on >= {from_unix}")
        return self._get_paginated(endpoint, *(*filters, *date_filters))

    def _get_paginated(self, endpoint: str, *filters, **kwargs) -> List[Dict]:
        params = {"start": 0, "limit": 4999}  # max limit, tested by trial and error
        while True:
            kwargs["params"] = params
            response = self._get(endpoint, *filters, **kwargs)
            content: Dict = response.json()
            resource_name = list(content.keys())[0]
            data = content[resource_name]
            if data:
                logger.info("Yielding %d rows", len(data))
                yield from data
                params["start"] += params["limit"]
            else:
                logger.info("No more data")
                break
            if params["start"] > 5005000:
                raise IndexError("Api is returning more than 5 million rows. Seems to be too many")


    def get_projects(self, *filters) -> List[Dict]:
        return self._get_paginated("projects", *filters)

    def get_tasks(self, *filters, from_: Optional[datetime] = None) -> List[Dict]:
        # return self._get_all("tasks", *filters)
        if from_ is None:
            from_ = datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
        return list(self.get_range("tasks", *filters, from_=from_))

    def get_entries(self, *filters, from_: Optional[datetime] = None) -> List[Dict]:
        ## Entries must be filtered
        if not filters:
            pids = [str(project["id"]) for project in self.get_projects()]
            pids_str = ", ".join(pids)
            filters = [f"project_id in ({pids_str})"]
        if from_ is None:
            from_ = datetime.now().replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
        return list(
            self.get_range(
                "entries",
                *filters,
                from_=from_,
            )
        )

    def get_users(self, *filters) -> List[Dict]:
        return self._get_paginated("users", *filters)

    def get_clients(self, *filters) -> List[Dict]:
        return self._get_paginated("clients", *filters)

    def get_tasklists(self, *filters) -> List[Dict]:
        return self._get_paginated("tasklists", *filters)

    def get_endpoint(self, endpoint: str, *filters, **kwargs) -> List[Dict]:
        if endpoint == "projects":
            return self.get_projects(*filters)
        if endpoint == "tasks":
            return self.get_tasks(*filters)
        if endpoint == "entries":
            return self.get_entries(*filters, **kwargs)
        if endpoint == "users":
            return self.get_users(*filters)
        if endpoint == "clients":
            return self.get_clients(*filters)
        if endpoint == "tasklists":
            return self.get_tasklists(*filters)
        raise NotImplementedError(f"Endpoint {endpoint} is not implemented.")

# BizzFlow Paymo Extractor

- **autor:** Petra Horackova (petra.horackova@bizztreat.com)
- **created:** 2019-10-20

## Description

Extractor for pulling the following data from Paymo App into .csv files:

- projects
- users
- tasks
- time entries (by previously downloaded project, user, and task separately)

## Requirements

Paymo API key
Pandas version 0.25 or higher

## How to use it

Create or modify `config.json` file. Use your API token as username value.
The request url is composed of base url, endpoint name, and filter query specified in config file, e.g. `https://app.paymoapp.com/api/api/entries?where=project_id=[PROJECT_ID]`. Modity the filter values in config file to specify which items should be downloaded. Filter for entries endpoint should not be modified as they are dynamically filled during runtime from previously received ID's for users/project/task respectively.

## Downloaded endpoints

If you do not wish to download time entries by each group (user, project, task), you may omit the key in the configuration file (see below).

## Configuration

See [`config.schema.json`](src/config.schema.json) or [json-editor](https://json-editor.github.io/json-editor/?data=N4Ig9gDgLglmB2BnEAuUMDGCA2MBGqIAZglAIYDuApomALZUCsIANOHgFZUZQD62ZAJ5gArlELwwAJzplsrEIgwALKrNSgAJEtXqUIZVCgQUAelMda8ALQ61ZAHTSA5qYAmUskSjWADAHZTO3U2KEEIKkIwTm5xNgQqAHkiVABtUCkqAEcRGEy3NJAANyopRDh4BSp4NwgwGHgoZFCwAGtqkABdAF8WDOzc/MKSsoqqmrqGpoURRFKFCDJERAppAp7OtggpSFLYGg1i0vKEQ7CIwimqZ3nQmChsSP0AUQAPKE8eaQACEZPKthuGgYKQwaBjfQAMR+eDIGFaFDIUjc3ywdEWsDwMFwYRY3yBRDIImwUBQ3wATAoCUSSahyWxqiI6GkAIwsck9FrtSpoEDnJ6KD4NZwKWAPAUAQQACgBJb4AFTaHUBwNB4NO+mlcqgSvg3xu8FKZCgVBRDW+whEUm+UqEdDA3zhWBEjRAvRAs3mvP5hEQQvgIru4sIAFU5tb4GQGFTVWDYBqQGHSt9IwxvjrvotBPbvgAKSRQFNUU2m74UVR62bC9O6gCUbq2SxWazO4QFftBAdF90ehFty1WyJjSjV8Z5IH7zeRNczdod+bAhcNJZR5eq3yrAZr3Pr7uqtXqjWQ3rbhCRnkE3eDLwmh+mbEyOTypsK+8mrs2IHuamPoB9+miLgeAUR9BhfFBUhAN87y6LYdgiKR9l/L9GlKIo5FbC4IJQk0bikBR4GJeRPzFXt9AAEWNKhviuKR0OwPMGhBNRqigesGVeKMIEeY9UgAZhYfwWBZAAWT9qWJcQUEI7BsBVEc4whEAADkmTwZMwCIfEhEQGcbkLNxjTIb4SGtXM6nKWASho1C8NrPEqAcZwHG+PiTJ+AQ/XTZRMmowzBGQPdbymTD239QM+R7AVnmC115JBRSExig8phnNwwAoeBsDAMgUVzOZHh4Eydjob5cD9dioMI5lsO2MAgPvPkllaZoqqFA42E9MoFAwXBWOQTliGxE1upPLCQHPIQrzIkBIWG45hwS9VxwAGRgLzNJM+ayjxOZqMMYxEDMUxnHuZQRDwBw0VMLN7TICAIFMe6YFMPBsrwUxZD9UogliCpEFMIhtuFBw6BRUzvntTIFG/OhkP/QVOwi0iBTmklbhAIEFOWwgAE1RGtIH0etDthSqTj0R4wosVksg3uogBeEy5DmbsGF4Wj6JsvMAB0QHJXwBb8Flhb5vE+YF8kRd8aWWT5hYdgajmUSZllyT4kTmDYVNGe+Pm6EEMr7nFTNFdieWem6K3rbYRBlAy3hSh2UacKNHgxlCXQBTwMBFw7e6RLdbogA==) for more information.

### Sample config

```json
{
  "$schema": "./src/config.schema.json",
  "version": 2,
  "token": "your-token",
  "endpoints": [
    {
      "endpoint": "entries",
      "interval": 1500
    },
    {
      "endpoint": "projects",
      "filters": ["billable = false"]
    }
  ]
}
```
